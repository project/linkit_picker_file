<?php

/**
 * @file
 *
 * Provides a handler that builds the string for file links we what to use with Linkit core.
 */
class linkit_picker_file_handler_field_file_info extends views_handler_field {

  function query() {
    _linkit_picker_set_active_profile();
  }

  function render($values) {
    $value = array(
      'path' => _linkit_picker_file_get_insert_plugin_processed_path('file/' . $values->fid),
      'title' => $values->file_managed_filename,
    );
    return drupal_json_encode($value);
  }

}
