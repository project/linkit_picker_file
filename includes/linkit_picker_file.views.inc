<?php

/**
 * @file
 * Provide views data for linkit_picker_file.module.
 */

/**
 * Implements hook_views_data().
 */
function linkit_picker_file_views_data() {
  // File field.
  $data['linkit_picker_file']['table']['group'] = t('Linkit Picker');
  $data['linkit_picker_file']['table']['join'] = array(
    'file_managed' => array(),
  );

  $data['linkit_picker_file']['linkit_picker'] = array(
    'group' => t('Linkit Picker'),
    'field' => array(
      'title' => t('Linkit Picker'),
      'help' => t('Provides info to Linkit Picker (files)'),
      'handler' => 'linkit_picker_file_handler_field_file_info',
    ),
  );

  return $data;
}
